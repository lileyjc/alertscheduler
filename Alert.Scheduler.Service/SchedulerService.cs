﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.Xpo;
using Quartz;
using Quartz.Impl;
using AlertSupport;

namespace AlertService
{
    class ScheduleService
    {
        private readonly IScheduler scheduler;

        public ScheduleService()
        {
            NameValueCollection props = new NameValueCollection
        {
            { "quartz.serializer.type", "binary" },
            { "quartz.scheduler.instanceName", "MyScheduler" },
            { "quartz.jobStore.type", "Quartz.Simpl.RAMJobStore, Quartz" },
            { "quartz.threadPool.threadCount", "3" },
            { "quartz.scheduler.exporter.type","Quartz.Simpl.RemotingSchedulerExporter, Quartz" },
            { "quartz.scheduler.exporter.port","555" },
            { "quartz.scheduler.exporter.bindName","QuartzScheduler"},
            { "quartz.scheduler.exporter.channelType","tcp"},
            { "quartz.scheduler.exporter.channelName","httpQuartz"},
            {"quartz.scheduler.exporter.rejectRemoteRequests","true" }
        };
            StdSchedulerFactory factory = new StdSchedulerFactory(props);
            scheduler = factory.GetScheduler().ConfigureAwait(false).GetAwaiter().GetResult();
        }
        public void Start()
        {
            //DBHelper helper = new DBHelper();
            //DBStatic.DataLayer = helper.GetDataLayer(106);
            
            scheduler.Start().ConfigureAwait(false).GetAwaiter().GetResult();

            ScheduleJobs();
        }
        public void ScheduleJobs()
        {

            if (DBStatic.DataLayer == null)
            {
                DBHelper helper = new DBHelper();
                DBStatic.DataLayer = helper.GetDataLayer(106);
            }

            using (UnitOfWork uow = new UnitOfWork(DBStatic.DataLayer))
            {

                XPCollection<Evolution.Data.ORM.AlertRule> xpc = new XPCollection<Evolution.Data.ORM.AlertRule>(uow);

                foreach (Evolution.Data.ORM.AlertRule ar in xpc)
                {

                    IJobDetail job = JobBuilder.Create<AlertJob>()
                   .WithIdentity(Convert.ToString(ar.Oid))
                   .UsingJobData("Oid", ar.Oid)
                   .Build();

                    ITrigger trigger = null;

                    switch (ar.Trigger.RecurrenceType)
                    {

                        case Evolution.Data.ORM.AlertTrigger.RecurrenceTypeOptions.Minutely:
                            trigger = TriggerBuilder.Create()
                                .WithIdentity(Convert.ToString(ar.Trigger.Oid))
                                .StartNow()
                                .WithSimpleSchedule(x => x
                                    .WithIntervalInMinutes(ar.Trigger.Periodicity)
                                    .RepeatForever())
                                .Build();
                            break;
                        case Evolution.Data.ORM.AlertTrigger.RecurrenceTypeOptions.Hourly:
                            trigger = TriggerBuilder.Create()
                               .WithIdentity(Convert.ToString(ar.Trigger.Oid))
                               .StartNow()
                               .WithSimpleSchedule(x => x
                                   .WithIntervalInHours(ar.Trigger.Periodicity)
                                   .RepeatForever())
                               .Build();
                            break;
                        case Evolution.Data.ORM.AlertTrigger.RecurrenceTypeOptions.Daily:
                            trigger = TriggerBuilder.Create()
                                 .WithIdentity(Convert.ToString(ar.Trigger.Oid))
                                 .StartNow()
                                 .WithCronSchedule(ar.Trigger.Schedule)
                                 .Build();
                            break;
                        default:
                            //trigger = TriggerBuilder.Create()
                            //    .WithIdentity(Convert.ToString(ar.Trigger.Oid))
                            //    .StartNow()
                            //    .WithSimpleSchedule(x => x
                            //        .WithIntervalInSeconds(10)
                            //        .RepeatForever())
                            //    .Build();
                            break;
                    }

                    // Tell quartz to schedule the job using our trigger
                    if (trigger != null)
                    {
                    scheduler.ScheduleJob(job, trigger).ConfigureAwait(false).GetAwaiter().GetResult();
                    }


                }

            }


            //IJobDetail job = JobBuilder.Create<HelloJob>()
            //    .WithIdentity("job1", "group1")
            //    .UsingJobData("Oid", 11)
            //    .Build();

            //ITrigger trigger = TriggerBuilder.Create()
            //    .WithIdentity("trigger1", "group1")
            //    .StartNow()
            //    .WithSimpleSchedule(x => x
            //        .WithIntervalInSeconds(10)
            //        .RepeatForever())
            //    .Build();

            //// Tell quartz to schedule the job using our trigger
            //scheduler.ScheduleJob(job, trigger).ConfigureAwait(false).GetAwaiter().GetResult();
        }

        public void Stop()
        {
            scheduler.Shutdown().ConfigureAwait(false).GetAwaiter().GetResult();
        }

   
    }

}
