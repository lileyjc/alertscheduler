﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientLibrary
{
    [DisallowConcurrentExecution]
    public class AlertJob : IJob
    {
        public Task Execute(IJobExecutionContext context)
        {

            //Log.Warning("Start Job");

            JobDataMap dataMap = context.JobDetail.JobDataMap;
            int oid = dataMap.GetIntValue("Oid");

            if (DBStatic.DataLayer == null)
            {
                DBHelper helper = new DBHelper();
                DBStatic.DataLayer = helper.GetDataLayer(106);
            }

            using (UnitOfWork uow = new UnitOfWork(DBStatic.DataLayer))
            {

                Evolution.Data.ORM.AlertRule rule = uow.GetObjectByKey<Evolution.Data.ORM.AlertRule>(oid);

                //DevExpress.DataAccess.Sql.SqlDataSource ds = new DevExpress.DataAccess.Sql.SqlDataSource(new MsSqlConnectionParameters("217.194.223.161,8484", "cst_test", "sa", "j1l5poUY4D8eZKa6", MsSqlAuthorizationType.SqlServer));
                DevExpress.DataAccess.Sql.SqlDataSource ds = new DevExpress.DataAccess.Sql.SqlDataSource(new MsSqlConnectionParameters("DESKTOP-445BHSV", "cst", "sa", "letmein59", MsSqlAuthorizationType.SqlServer));

                ds.LoadFromXml(XElement.Parse(rule.Query.ResultQuery));
                ds.Fill();
                DevExpress.DataAccess.Sql.DataApi.ITable table = ds.Result[0];

                foreach (DevExpress.DataAccess.Sql.DataApi.IRow row in table)
                {
                    Evolution.Data.ORM.Alert alert = new Evolution.Data.ORM.Alert(uow);
                    alert.AlertType = rule.AlertType;
                    alert.ObjectID = (int)row["Oid"];
                    alert.Description = string.Format("Product {0} has fallen below min stock level", (string)row["ProductID"]);
                    alert.Rule = rule;
                    alert.Assignee = rule.Assignee;
                }



                //'SelectQuery query = (SelectQuery)ds.Queries[0];
                //'ds.Connection.Open();
                //'DBSchema schema = ds.Connection.GetDBSchema();

                //'Log.Warning(query.GetSql(schema));

                //Evolution.Data.ORM.Alert alert = new Evolution.Data.ORM.Alert(uow);
                //alert.Rule = rule;
                //alert.Assignee = rule.Assignee;

                uow.CommitChanges();

            }

            var lastRun = context.PreviousFireTimeUtc?.DateTime.ToString() ?? string.Empty;
            //Log.Warning("End Job!   Previous run: {lastRun}", lastRun);
            return Task.CompletedTask;
        }

    }

}
