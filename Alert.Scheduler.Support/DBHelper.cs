﻿using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlertSupport
{
    public class DBHelper
    {
        public IDataLayer GetDataLayer(int company)
        {

            DevExpress.Xpo.Metadata.XPDictionary dict = new DevExpress.Xpo.Metadata.ReflectionDictionary();

            // Initialize the XPO dictionary.
            dict.GetDataStoreSchema(typeof(Evolution.Data.ORM.Company).Assembly);

            DevExpress.Xpo.DB.ConnectionProviderSql store = (DevExpress.Xpo.DB.ConnectionProviderSql)DevExpress.Xpo.XpoDefault.GetConnectionProvider(GetConnectionString("cst"), DevExpress.Xpo.DB.AutoCreateOption.SchemaOnly);

            ThreadSafeDataLayer dl = new ThreadSafeDataLayer(dict, store);

            return dl;

        }

        private string GetConnectionString(string dbName)
        {
            //return DevExpress.Xpo.DB.MSSqlConnectionProvider.GetConnectionString("217.194.223.161,8484", "sa", "j1l5poUY4D8eZKa6", dbName);
            return DevExpress.Xpo.DB.MSSqlConnectionProvider.GetConnectionString("DESKTOP-445BHSV", "sa", "letmein59", dbName);

        }
    }
}
