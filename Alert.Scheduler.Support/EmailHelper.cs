﻿using AlertSupport;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace AlertSupport
{

    class EmailHelper
    {
        private string _server;
        private int _port;

        public EmailHelper(string server,int port = 25)
        {
            _server = server;
            _port = port;
        }
        public void SendEmail(String receipients, String subject, String body)
        {
            MailMessage mail = new MailMessage();

            mail.From = new MailAddress("lileyjc@hotmail.com", "James Liley");
            mail.To.Add(new MailAddress("lileyjc@hotmail.com"));

            mail.Subject = subject;
            mail.Body = body;

            SmtpClient smtp = new SmtpClient(_server,_port);

            smtp.Send(mail);

        }

    }
}
