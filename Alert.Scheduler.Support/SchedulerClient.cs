﻿using DevExpress.Xpo;
using Evolution.Data.ORM;
using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlertSupport
{
    public class SchedulerClient
    {

            public readonly IScheduler Instance;
            public string Address { get; private set; }
            public string JobName { get; set; }
            public string JobGroup { get; set; }
            public int Priority { get; set; }
            public string CronExpression { get; set; }

            private readonly ISchedulerFactory _schedulerFactory;

            public SchedulerClient(string server, int port, string scheduler)
            {
                Address = string.Format("tcp://{0}:{1}/{2}", server, port, scheduler);
                _schedulerFactory = new StdSchedulerFactory(GetProperties(Address));

            try
            {
                //Instance = _schedulerFactory.GetScheduler();
                Instance = _schedulerFactory.GetScheduler().ConfigureAwait(false).GetAwaiter().GetResult();

                if (!Instance.IsStarted)
                        Instance.Start();
                }
                catch (SchedulerException ex)
                {
                    throw new Exception(string.Format("Failed: {0}", ex.Message));
                }
            }

            private static NameValueCollection GetProperties(string address)
            {
                var properties = new NameValueCollection();
                properties["quartz.scheduler.instanceName"] = "MyScheduler";
                properties["quartz.scheduler.proxy"] = "true";
                properties["quartz.threadPool.threadCount"] = "0";
                properties["quartz.scheduler.proxy.address"] = address;
                return properties;
            }

            public IScheduler GetScheduler()
            {
                return Instance;
            }

        public async void AddAlertJob(int alertID)
        {

            if (DBStatic.DataLayer == null)
            {
                DBHelper helper = new DBHelper();
                DBStatic.DataLayer = helper.GetDataLayer(106);
            }


            using (UnitOfWork uow = new UnitOfWork(DBStatic.DataLayer))
            {

                AlertRule ar = uow.GetObjectByKey<AlertRule>(alertID);

                IJobDetail jd;

                jd = await GetScheduler().GetJobDetail(new JobKey(ar.Oid.ToString()));

                if (jd != null)
                {

                    Boolean result = await GetScheduler().DeleteJob(new JobKey(ar.Oid.ToString()));
                }
                
                jd = JobBuilder.Create<AlertJob>()
                    .WithIdentity(ar.Oid.ToString())
                    .UsingJobData("Oid", ar.Oid)
                    .Build();

                ITrigger trigger;

                switch (ar.Trigger.RecurrenceType)
                {
                    case Evolution.Data.ORM.AlertTrigger.RecurrenceTypeOptions.Minutely:
                        trigger = TriggerBuilder.Create()
                            .WithIdentity(Convert.ToString(ar.Trigger.Oid))
                            .StartNow()
                            .WithSimpleSchedule(x => x
                                .WithIntervalInMinutes(ar.Trigger.Periodicity)
                                .RepeatForever())
                            .Build();
                        break;
                    case Evolution.Data.ORM.AlertTrigger.RecurrenceTypeOptions.Hourly:
                        trigger = TriggerBuilder.Create()
                           .WithIdentity(Convert.ToString(ar.Trigger.Oid))
                           .StartNow()
                           .WithSimpleSchedule(x => x
                               .WithIntervalInHours(ar.Trigger.Periodicity)
                               .RepeatForever())
                           .Build();
                        break;
                    case Evolution.Data.ORM.AlertTrigger.RecurrenceTypeOptions.Daily:
                        trigger = TriggerBuilder.Create()
                             .WithIdentity(Convert.ToString(ar.Trigger.Oid))
                             .StartNow()
                             .WithCronSchedule(ar.Trigger.Schedule)
                             .Build();
                        break;
                    default:
                        trigger = TriggerBuilder.Create()
                            .WithIdentity(Convert.ToString(ar.Trigger.Oid))
                            .StartNow()
                            .WithSimpleSchedule(x => x
                                .WithIntervalInSeconds(10)
                                .RepeatForever())
                            .Build();
                        break;
                }

                Instance.ScheduleJob(jd, trigger).ConfigureAwait(false).GetAwaiter().GetResult();

            }

        }

    }

}
