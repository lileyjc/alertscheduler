﻿using DevExpress.Data.Filtering;
using DevExpress.DataAccess.ConnectionParameters;
using DevExpress.DataAccess.Sql;
using DevExpress.Xpo;
using Evolution.Data.ORM;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace AlertSupport
{
    [DisallowConcurrentExecution]
    public class AlertJob : IJob
    {
        public System.Threading.Tasks.Task Execute(IJobExecutionContext context)
        {

            //Log.Warning("Start Job");

            EmailHelper EventHandler = new EmailHelper("VPSBACKEND", 25);

            JobDataMap dataMap = context.JobDetail.JobDataMap;
            int oid = dataMap.GetIntValue("Oid");

            if (DBStatic.DataLayer == null)
            {
                DBHelper helper = new DBHelper();
                DBStatic.DataLayer = helper.GetDataLayer(106);
            }

            using (UnitOfWork uow = new UnitOfWork(DBStatic.DataLayer))
            {


                Evolution.Data.ORM.AlertRule rule = uow.GetObjectByKey<Evolution.Data.ORM.AlertRule>(oid);

                //DevExpress.DataAccess.Sql.SqlDataSource ds = new DevExpress.DataAccess.Sql.SqlDataSource(new MsSqlConnectionParameters("217.194.223.161,8484", "cst_test", "sa", "j1l5poUY4D8eZKa6", MsSqlAuthorizationType.SqlServer));
                DevExpress.DataAccess.Sql.SqlDataSource ds = new DevExpress.DataAccess.Sql.SqlDataSource(new MsSqlConnectionParameters("DESKTOP-445BHSV", "cst", "sa", "letmein59", MsSqlAuthorizationType.SqlServer));

                ds.LoadFromXml(XElement.Parse(rule.Query.ResultQuery));

                foreach (QueryParameter qp in ds.Queries[0].Parameters)
                {
                    
                    AlertParameter ap = uow.FindObject<AlertParameter>(new GroupOperator(new BinaryOperator("Rule",rule,BinaryOperatorType.Equal),new BinaryOperator("Name",qp.Name,BinaryOperatorType.Equal)));
                 
                    if (ap != null)
                    {
                        switch (qp.Type.Name)
                        {
                            case "String":
                                qp.Value = ap.Value;
                                break;
                            case "DateTime":
                                qp.Value = DateTime.ParseExact(ap.Value, "dd/MM/yyyy", null);
                                break;
                            case "Decimal":
                                qp.Value = Convert.ToDecimal(ap.Value);
                                break;
                            case "Int32":
                                qp.Value = Convert.ToInt32(ap.Value);
                                break;
                        }
                    }
                }


                ds.Fill();

                DevExpress.DataAccess.Sql.DataApi.ITable table = ds.Result[0];

                foreach (DevExpress.DataAccess.Sql.DataApi.IRow row in table)
                {

                    Alert alert = uow.FindObject<Alert>(new GroupOperator(
                        new BinaryOperator("Rule",rule,BinaryOperatorType.Equal),
                        new BinaryOperator("ObjectID",(int)row["Oid"],BinaryOperatorType.Equal),
                        new BinaryOperator("Completed",false,BinaryOperatorType.Equal)));

                    if (alert == null)
                    {
                        int objectID = (int)row["Oid"];
                        alert = new Evolution.Data.ORM.Alert(uow);
                        alert.AlertType = rule.AlertType;
                        alert.ObjectID = objectID;
                        //alert.Description = string.Format("Product {0} has fallen below min stock level", (string)row["ProductID"]);
                        alert.Rule = rule;
                        alert.Assignee = rule.Assignee;

                        switch (alert.AlertType)
                        {
                            case Alert.AlertTypes.Customer:
                                Customer c = uow.GetObjectByKey<Customer>(objectID);
                                alert.Reference1 = c.CustomerID;
                                alert.Reference2 = c.Name;
                                break;
                            case Alert.AlertTypes.Product:
                                Product p = uow.GetObjectByKey<Product>(objectID);
                                alert.Reference1 = p.ProductID;
                                alert.Reference2 = p.Name;
                                break;
                            case Alert.AlertTypes.Supplier:
                                Supplier s = uow.GetObjectByKey<Supplier>(objectID);
                                alert.Reference1 = s.SupplierID;
                                alert.Reference2 = s.Name;
                                break;
                            case Alert.AlertTypes.User:
                                break;
                            case Alert.AlertTypes.WarehouseProduct:
                                WarehouseProduct wp = uow.GetObjectByKey<WarehouseProduct>(objectID);
                                alert.Reference1 = string.Format("{0}/{1}",wp.WarehouseID,wp.ProductID);
                                alert.Reference2 = wp.Product.Name;
                                break;
                            case Alert.AlertTypes.WarehouseStock:
                                StockItem stockItem = uow.GetObjectByKey<StockItem>(objectID);
                                alert.Reference1 = string.Format("{0}/{1}/{2}",stockItem.WarehouseProduct.WarehouseID,stockItem.WarehouseProduct.ProductID,stockItem.Batch);
                                alert.Reference2 = stockItem.WarehouseProduct.Product.Name;
                                break;
                        }


                    }


                }

                

                //'SelectQuery query = (SelectQuery)ds.Queries[0];
                //'ds.Connection.Open();
                //'DBSchema schema = ds.Connection.GetDBSchema();

                //'Log.Warning(query.GetSql(schema));

                //Evolution.Data.ORM.Alert alert = new Evolution.Data.ORM.Alert(uow);
                //alert.Rule = rule;
                //alert.Assignee = rule.Assignee;

                uow.CommitChanges();

            }

            var lastRun = context.PreviousFireTimeUtc?.DateTime.ToString() ?? string.Empty;
            //Log.Warning("End Job!   Previous run: {lastRun}", lastRun);
            return System.Threading.Tasks.Task.CompletedTask;
        }

    }

}
